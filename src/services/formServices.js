const services = {
  salvarTarefa(props) {
    var tasks = localStorage.getItem("tasks")
      ? JSON.parse(localStorage.getItem("tasks"))
      : [];

    if (!props.form.subject) {
      return "O Título é obrigatório!";
    }

    if (props.methodSalvar === "update") {
      tasks[props.$route.params.index] = props.form;
      localStorage.setItem("tasks", JSON.stringify(tasks));
      props.$router.push({ name: "list" });
      return;
    }

    tasks.push(props.form);
    localStorage.setItem("tasks", JSON.stringify(tasks));
    props.$router.push({ name: "list" });
  },
};

export default services;
