const services = {
  editarTarefa(props, index) {
    props.$router.push({ name: "form", params: { index } });
  },

  excluirTarefa(task, index) {
    this.taskSelected = task;
    this.taskSelected.index = index;
  },
  removerTarefa(task, arrayTask) {
    arrayTask.splice(this.taskSelected.index, 1);
    localStorage.setItem("tasks", JSON.stringify(arrayTask));
  },
  concluirTarefa(task, index, props) {
    let tasks = JSON.parse(localStorage.getItem("tasks"));

    tasks[index] = task;
    localStorage.setItem("tasks", JSON.stringify(tasks));
    props.$router.push({ name: "list" });
    return;
  },
};

export default services;
